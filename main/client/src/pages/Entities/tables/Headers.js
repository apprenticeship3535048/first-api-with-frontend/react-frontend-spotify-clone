export function headers(array) {
    for (let i = 0; i < array.length; i++) {
        switch (array[i]){
            case "id":
                array[i] = <th key="id-head">ID</th>
                break;
            case "name":
                array[i] = <th key="name-head">Name</th>
                break;
            case "yearFounded":
                array[i] = <th key="yf-head">Year <p>Founded</p> </th>
                break;
            case "label":
                array[i] = <th key="label-head">Label</th>
                break;
            case "numOfReleases":
                array[i] = <th key="noR-head">Number <p>of Releases</p></th>
                break;
            case "createdAt":
                array[i] = <th key="ca-head">Created At</th>
                break;
            case "updatedAt":
                array[i] = <th key="ua-head">Updated At</th>
                break;
            case "numberOfSales":
                array[i] = <th key="nos-head">Sold</th>
                break;
            case "release_year":
                array[i] = <th key="yr-head">Year Released</th>
                break;
            case "numOfSongs":
                array[i] = <th key="song-head">Number <p>of Songs</p></th>
                break;
            case "length":
                array[i] = <th key="l-head">Length (s)</th>
                break;
            case "numOfBands":
                array[i] = <th key="bands-head">Number <p>of Bands</p></th>
                break;
            case "release":
                array[i] = <th key="release-head">Release</th>
                break;
            case "positionId":
                array[i] = <th key="position-head">Position</th>
                break;
            case "band":
                array[i] = <th key="band-head">Artist</th>
                break;
            case "roles":
                array[i] = <th key="band-head">Roles</th>
                break;
            default:
                delete array[i]
                console.log("no match")
                break;
        }
    }
    return array;
}