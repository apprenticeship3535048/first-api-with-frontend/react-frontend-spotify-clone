import React, {useEffect, useState} from "react";

import axios from "axios";
import ListOfChoices from "../../index/dashboard/ListOfChoices";
import {useBootstrapBreakpoints} from "react-bootstrap/ThemeProvider";

export default function Post({entity, setPosting}) {
    let popUp = []
    const [wasSelected, setWasSelected] = useState(false);
    const columns = entity.columns;
    //placeHolder Search
    //Popup Band hat noch keine
    //Warning für unvollständiges Label
    const [firstValue, setFirstValue] = useState("")
    const [secondValue, setSecondValue] = useState("")
    const [thirdValue, setThirdValue] = useState("")
    const [fourthValue, setFourthValue] = useState("")

    const [search, setSearch] = useState("");
    const [results, setResults] = useState([])

    const setFirstValueHandler = (e) => {
        setFirstValue(e.target.value)
    }

    const setSecondValueHandler = (e) => {
        setSecondValue(e.target.value)
    }

    const setThirdValueHandler = (e) => {
        setThirdValue(e.target.value)
    }

    const setFourthValueHandler = (e) => {
        setFourthValue(e.target.value)
    }

    const setSearchValue = (e) => {
        setSearch(e.target.value)
    }

    const openListHandler = () => {
        document.getElementById(entity.parent + "-list").style.display = "flex";
        document.getElementById(entity.parent + "-list").onclick = closeListHandler;
    }

    const closeListHandler = () => {
        document.getElementById(entity.parent + "-list").style.display = "none";
    }

    let handlerArray = [setFirstValueHandler, setSecondValueHandler, setThirdValueHandler, setFourthValueHandler]

    useEffect(() => {
        if (!search || search === "" || wasSelected) {
            setWasSelected(false);
            setResults(null)
            return

        } else {
            document.getElementById(entity.parent + "-list").style.display = "flex";
        };

        if (entity.parent != null) {
            axios.get("http://localhost:8080/api/" + entity.parent.toLowerCase() + "s/mapped-search", {
                params: {
                    name: search ? search : "",
                    limit: 5,
                    page: 1
                }

            }).then(res => {
                setResults(res.data.content);

            }).catch(e => console.log(e))
        }
    }, [search])

    function resetValues() {
        setFirstValue(null);
        setSecondValue(null);
        setThirdValue(null);
        setPosting(true);

        columns.map(column => {
            document.getElementById(column.columnTitle + "-input").value = null;
        })
    }

    function getParentId() {
        return new Promise((resolve) => {
            if (entity.parent != null) {
                let input = document.getElementById(entity.parent + "Id-input");
                input.style.backgroundColor = "white";

                axios.get("http://localhost:8080/api/" + entity.parent.toLowerCase() + "s/mapped-search", {
                    params: {
                        name: search ? search : "",
                        limit: 5,
                        page: 1
                    }

                }).then(res => {
                    let results = res.data.content;

                    if (input.value.length > 0 && results.length > 0) {
                        for (let x = 0; x < results.length; x++) {

                            if (results[x].name === input.value) {
                                resolve(results[x].id);
                                break;
                            }

                            if (x < results.length - 1) {
                                continue;
                            }

                            input.style.backgroundColor = "red";
                            console.error(entity.parent + " has no Matches, make sure that everything is spelled correctly")
                        }

                    } else if (results.length > 0) {
                        resolve(null)

                    } else {
                        input.style.backgroundColor = "red";
                        console.error(entity.parent + " has no Matches, make sure that everything is spelled correctly")
                    }
                })
            } else {
                resolve(null);
            }
        })
    }

    async function post() {
        let values = [firstValue, secondValue, thirdValue, fourthValue]
        for (let x = 0; x < columns.length; x++) {
            document.getElementById(columns[x].columnTitle + "-input").style.backgroundColor = "white";

            if (columns[x].required && (values[x] == null || values[x] === "")) {

                console.error(columns[x].columnTitle + " input is required")
                document.getElementById(columns[x].columnTitle + "-input").style.backgroundColor = "red";
                return;
            }
        }

        axios.post("http://localhost:8080/api/" + entity.type.toLowerCase() + "s",
            entity.postValue(await getParentId(), firstValue, secondValue, thirdValue, fourthValue)
        ).then(res => console.log(res.data)).catch(res => console.log(res)).then(resetValues)
    }

    const selectChoiceHandler = event => {
        setWasSelected(true);
        setSearch(event.target.value);
    }

    function fillPost() {
        let i = 0;
        let children = [];

        columns.map(column => {
            children[i] = [
                <h5 style={{
                    width: "35%", marginLeft: "3px",
                    color: (column.required ? "white" : "rgb(180, 180, 180)"),
                }}
                    key={column.columnTitle + "-Child"}>
                    {(column.required ? "* " : "") + column.columnTitle}
                </h5>,
                (column.columnType !== "radio") ?
                    <input
                        type={column.columnType}
                        key={column.columnTitle + "-input"}
                        id={column.columnTitle + "-input"}
                        className="ms-5 mt-1 mb-1"
                        style={{width: "50%"}}
                        name={column.columnTitle}
                        onChange={handlerArray[i]}
                        min={column.min}
                        max={column.max}
                    /> :
                    <div style={{marginLeft: "17%", width: "45%"}}>
                        {column.radioOptions.map(option =>
                            <div style={{overflow: "auto", display: "flex", justifyContent: "space-between"}}>
                                <h6 key={columns[i].columnTitle + "-" + option.name + "-text"} style={{float: "right"}}>
                                    {option.name}
                                </h6>
                                <div style={{width: "3rem"}} className={"form-check-outerBox"}>
                                    <div className={"form-check-innerBox"} />
                                    <input type="radio"
                                           id={columns[i].columnTitle + "-input"}
                                           key={columns[i].columnTitle + "-" + option.name + "-input"}
                                           name={columns[i].columnTitle + "-input"}
                                           value={option.value}
                                           style={{float: "right"}}
                                           onChange={handlerArray[i]}
                                           className={"form-check-input"}
                                    />
                                </div>
                            </div>
                        )}
                    </div>
            ]
            popUp[i] = React.createElement('div',
                {className: "d-flex flex-row border-1-whitesmoke ms-2 mt-2", key: columns[i].columnTitle + "-div"},
                children[i++])
        })

        if (entity.parent != null) {
            children[i] = [
                <h5 style={{
                    width: "35%", marginLeft: "3px",
                    color: "rgb(180, 180, 180)",
                }}
                    key={entity.parent + "Id-Child"}>
                    {entity.parent}
                </h5>,
                <input
                    type="text"
                    placeholder="search"
                    autocomplete="off"
                    key={entity.parent + "Id-input"}
                    id={entity.parent + "Id-input"}
                    className="ms-5 mt-1 mb-1"
                    style={{width: "50%"}}
                    name={entity.parent + "Id"}
                    onChange={setSearchValue}
                    value={search}
                    onClick={openListHandler}
                />,
                <ListOfChoices id={entity.parent + "-list"} options={results} onClick={selectChoiceHandler}/>
            ]

            popUp[i] = React.createElement('div',
                {className: "d-flex flex-row border-1-whitesmoke ms-2 mt-2", key: columns.parent + "-div"},
                children[i])
        }

        return popUp;
    }


    return (

        <div className="d-flex flex-column border-2-whitesmoke mt-3 mb-3 ms-5 bg-darkish"
             style={{width: "25vw", height: "max-content", color: "whitesmoke"}}>
            <h2 className="border-2-whitesmoke text-center">ADD NEW {entity.type.toUpperCase()}</h2>
            {fillPost()}
            <p style={{height: "10px"}}>
                <span style={{
                    float: "right",
                    marginRight: "5px",
                    color: "rgb(190, 190, 190)",
                    fontWeight: 600,
                    fontSize: "0.8rem"
                }}>*required</span>
            </p>
            <button className="edit-button border-1-whitesmoke ms-5 me-5 mt-2 mb-3" onClick={function () {
                post()
            }}>
                SAVE
            </button>
        </div>

    )
}