import Buttons from "../../index/dashboard/Buttons.css"
import style from "./style.css"
import React, {useEffect, useState} from "react";
import Post from "./Post";
import axios from "axios";
import {EntityTypes} from "../../index/dashboard/ENUM/EntityTypes";
import {headers} from "./Headers";
import Popup from 'reactjs-popup'

export default function TableView({entity}) {
    const buttonClassList = "border-2-whitesmoke edit-button ms-2 pe-2";
    const [page, setPage] = useState()
    const [pageNum, setPageNum] = useState(1)
    const [table, setTable] = useState([<div>Nothing to display</div>])
    const [tableInfo, setTableInfo] = useState([<div>Nothing to display</div>])
    const [posting, setPosting] = useState(true)

    useEffect(() => {
        setPageNum(1)
    }, [entity])

    useEffect(() => {
        axios.get("http://localhost:8080/api/" + entity.type.toLowerCase() + "s/mapped", {
            params: {
                limit: 10,
                page: pageNum ? pageNum : 1
            }
        }).then(res => {
            setPage(res.data)

        }).catch(err => {
            console.log(err)
        })

        if(posting) {
            setPosting(false);
        }
    }, [pageNum, entity, posting]);

    const getId = e => {
        console.log(e.target.id)
    } //TODO: Delete function

    const bandsHeaders = () => {
        if (!page) return;
        if (page.content.length === 0) {
            if (pageNum !== 1) {
                setPageNum(1);
            } else if (page.pageable.pageNumber !== 1){
                page.pageable.pageNumber = 1;
                setPageNum(1);
            }
            return;
        }

        console.log(page)

        let columnHeaders = headers(Object.keys(page.content[0]))
        return columnHeaders;
    }

    useEffect(()=> {
        if (!page) return;
        if (page.content.length === 0) {
            if (pageNum !== 1) {
                setPageNum(1);
            }
            return;
        }

        let jsonData = page.content
        let returnArray = [];
        let keys = Object.keys(page.content[0]);

        for (let i = 0; i < page.numberOfElements; i++) {
            let children = [];
            for (let j = 0; j < keys.length; j++) {
                children[j] = createTableData(keys[j],jsonData[i])
            }

            returnArray[i] = React.createElement('tr', {className:"border-b-1-whitesmoke", height:"60vh"}, children,<td style={{width: "20px"}}>
                    <button className="ms-2 edit-button border-1-whitesmoke" key={i}>EDIT</button>
                </td>,
                <td style={{width: "20px"}}>
                    <button className="delete-button border-1-whitesmoke" style={{alignSelf: "end"}} key={i} onClick={getId}>DEL</button>
                </td>)
        }
        setTable(returnArray);

    }, [page])

    function createTableData(key, jsonData){
        let child;

        switch (key) {
            case "label":
                if (jsonData[key] !== null) child = React.createElement('td', null, jsonData[key].name)
                else child = React.createElement('td', null, "#NOENTRY")
                break;

            case "roles":
                let childChild = []
                for (let i = 0; i < jsonData[key].length; i++) {
                    console.log(jsonData[key][i].name)
                    if (jsonData[key].length > 1 && i !== jsonData[key].length-1) childChild[i] = React.createElement('div', null,jsonData[key][i].name + ", ")
                   else childChild[i] = React.createElement('div', null,jsonData[key][i].name)
                }
                child = React.createElement('td', {key:"roles"},childChild)
                break;

            case "createdAt":

            case "updatedAt":
                let date = new Date(jsonData[key])
                child = React.createElement('td', null,
                    date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear() +
                    " " + date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+"")
                break;

            case "length":
                child = React.createElement('td',null, (jsonData[key]/60).toFixed(2))
                break;

            default:
                child = React.createElement('td',null, jsonData[key])
                break;
        }
        return child;
    }

    useEffect(()=> {
        if (!page) return;

        const currentPage = page.pageable.pageNumber
        let children = [];
        const next = <button key="next" className="border-1-whitesmoke edit-button ms-2 mt-2" onClick={function (){setPageNum(currentPage +1)}}>NEXT</button>
        const previous = <button key="prev" className="border-1-whitesmoke edit-button ms-2 mt-2" onClick={function (){setPageNum(currentPage -1)}}>PREV</button>
        if (currentPage === 1 && page.totalPages !== 1) children = [next]
        else if (page.totalPages === 1) children = []
        else if (currentPage === page.totalPages && page.totalPages !== 1) {
            console.log(page.totalPages)
            children = [previous]
        }
        else children = [previous, next]

        setTableInfo(<div key="footerInfo">
            Page: {page.pageable.pageNumber} of {page.totalPages},
            showing {page.numberOfElements} of {page.totalElements} elements.
            Entries shown per Page: {page.pageable.pageSize}
            {children}
        </div>)
    }, [page])

    function getTrigger(){
        return (
            <button key="PostButton" className={buttonClassList}>
                <h1>+</h1>
            </button>
        )
    }

    return (
        <div className="boxShadow blackBorderTop bg-darkish d-flex flex-row"
            style={{
                width: "calc(100vw - 150px)",
                height: "80vh",
                overflowY: "auto",
                color: "whitesmoke"
            }}>

            <div className="ms-5 mt-3 w-100">
                <table style={{minWidth: "55vw", width: "97%"}}>
                    <thead>
                    <tr style={{verticalAlign: "top"}} >
                        {bandsHeaders()}
                        <th colSpan={2}>
                            <Popup trigger={getTrigger} arrow={false} position={'bottom right'}>
                                <Post entity={entity} setPosting={setPosting}/>
                            </Popup>

                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {table}
                    </tbody>
                </table>
                {tableInfo}

            </div>
        </div>
    )
}