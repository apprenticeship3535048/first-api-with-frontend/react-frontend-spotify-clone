import React, {useEffect, useState} from "react";
import axios from "axios";
import {LengthCalculator} from "../../../index/dashboard/spotifyComponents/controls/LengthCalculator";
import shadow from "../../../index/dashboard/shadow.css"
import style from "../style.css"
import {forEach} from "react-bootstrap/ElementChildren";
import placeholder from "./loading_transparent.gif"

export default function Band({spotifyWebApi, chooseTrack}) {

    const loading = <div style={{
        height: "200px",
        width: "200px",
        borderRadius: "50%",
        backgroundColor: "rgb(0,0,0,0.2)"
    }}>
        <img src={placeholder} style={{
            height: "50px",
            width: "50px",
            position: "relative",
            top: "75px",
            left:"75px"
        }}/>
    </div>


    const [band, setBand] = useState()
    const [bandCover, setBandCover] = useState(loading)
    const [releases, setReleases] = useState()

    useEffect(() => {
        axios
            .get("http://localhost:8080/api/bands/4/mapped")
            .then(res => {
                setBand(res.data)
                console.log(res.data)

            }).catch(e => console.log(e))
    }, [])

    useEffect(() => {
        if (!band) return
        setReleases(band.releases.content)
    }, [band])

    function releaseView() {
        if (!releases) return

        const releaseElements = []
        for (let i = 0; i < releases.length; i++) {

            const songList = releases[i].songs.content
            let songs;
            if (songList.length > 0) songs = setSongs(songList, releases[i])

            const genreList = releases[i].genres
            let genres = []
            if (genreList.length > 0) genres = listOfNestedNamesAsHTML(genreList)

            releaseElements[i] = [setReleaseElements(songList, releases[i], genres, i), makeSongTable(songs, releases[i].label)]
        }

        return releaseElements;
    }

    // these next four methods are used in releaseView
    function makeSongTable(songs, label) {
        return (
            <div className="ms-4 mt-3">
            <table  style={{width: "90%"}}>
                <thead className="border-b-1-whitesmoke" style={{fontSize: "0.9em", color: "rgb(255,255,255, 0.4)"}}>
                <th style={{height: "4vh", width: "3vw"}}>#</th>
                <th>Song</th>
                <th style={{textAlign: "right", paddingRight: "2vw"}}>Length</th>
                </thead>
                <tbody>
                {songs}
                </tbody>
            </table>

                <div style={{textAlign: "right", width:"90%", color:"rgb(255,255,255, 0.4)", fontSize: "0.75em"}} >{label ? label.name : ""}</div>
            </div>
        )
    }

    function setSongs(songList, release) {
        return songList.map((song) =>

            <tr style={{height: "5vh"}} className="border-b-1-whitesmoke song-hover"
                onClick={(e) => {
                playSong(song.name, nestedNamesAsString(release.artists))
                }}>
                <td>{song.positionId}</td>
                <td>
                    <div
                         style={{cursor: "default"}}>{song.name}</div>
                    <tr style={{fontSize: "0.75em"}}
                        className="d-flex flex-row">{listOfNestedNamesAsHTML(release.artists)}</tr>
                </td>
                <td style={{textAlign: "right", paddingRight: "2vw"}}>{LengthCalculator(song.length)}</td>
            </tr>
        )
    }

    function getTotalAlbumLength(songList) {
        let sumOfSeconds = 0;
        songList.forEach((song) => sumOfSeconds = sumOfSeconds + song.length)
        return sumOfSeconds
    }

    function setReleaseElements(songList, release, genres, i) {
        return React.createElement('div', {key: i + "hi", className: "border-b-1-whitesmoke mt-3 me-5"},
            <h2 className="ms-3">{release.name}</h2>,
            <div className="d-flex" style={{width: "100%"}}>
                <div className="d-flex flex-row" style={{width: "25%", color: "rgb(255,255,255, 0.4)"}}>
                    <h6 className="ms-3">
                        {release.release_year ? <>{release.release_year} &bull;&nbsp;</> : ""}
                        {release.type} &bull;&nbsp;
                        {songList.length} songs &bull;&nbsp;
                        {LengthCalculator(getTotalAlbumLength(songList))}
                    </h6>

                </div>
                <div className="d-flex flex-row justify-content-end" style={{width: "75%", marginRight: "2vw"}}>
                    {genres}
                </div>
            </div>,
        )
    }

    function listOfNestedNamesAsHTML(nestedObjects) {
        const names = []

        for (let j = 0; j < nestedObjects.length; j++) {

            if (j === nestedObjects.length - 1) names[j] = <h6 style={{color: "gray"}}>{nestedObjects[j].name}</h6>
            else names[j] = <h6 style={{color: "gray"}}>{nestedObjects[j].name + " -"}&nbsp;</h6>
        }
        return names
    } // this will return the names of genres or bands nested in releases
    function nestedNamesAsString(nestedObjects) {
        let names = ""

        for (let j = 0; j < nestedObjects.length; j++) {
            names = names + nestedObjects[j].name + " "
        }
        return names
    }

    function playSong(title, artists) {
        const searchString = title + " " + artists
        console.log(searchString)
        spotifyWebApi.searchTracks(searchString, {limit: 1, offset: 0}).then(res => {
                console.log(res.body.tracks)
                chooseTrack(res.body.tracks.items[0])
            }
        )
    }

    useEffect(() => {
        if (!band) return;

        spotifyWebApi.searchTracks(band.name + " " + band.releases.content[0].name, {limit: 1, offset: 0}).then(res => {
            let bandPos = 0;
            const artists = res.body.tracks.items[0].artists

            for (let i = 0; i < artists.length; i++) {
                if (band.name === artists[i].name) bandPos = i
            }
            spotifyWebApi.getArtist(artists[bandPos].id).then(res => {

                if (res.body.name === band.name) {
                    setBandCover(
                        <div className="rounded-circle"
                             style={{
                                 height: "200px",
                                 width: "200px",
                                 backgroundImage: 'url(' + res.body.images[0].url + ')',
                                 backgroundSize: "cover"
                             }}>

                        </div>)

                } else {
                    const bandInitials = band.name.slice(0, 2).toUpperCase()
                    setBandCover(
                        <div className="d-flex rounded-circle justify-content-center"
                             style={{
                                 height: "200px",
                                 width: "200px",
                                 fontSize:"5em",
                                 backgroundColor: "rgb(0,0,0,0.2)",
                             }}>
                            <div style={{
                                margin: "auto"
                            }}>{bandInitials}</div>

                        </div>)
                }
            })
        })

    }, [band])

    return (
        <div style={{
            backgroundColor: "#2b2b2b",
            width: "calc(100vw - 150px)",
            height: "80vh",
            overflowY: "auto",
            color: "whitesmoke"
        }}

             className="d-flex flex-column boxShadow blackBorderTop"
        >
            <div className="ms-4 mt-5">
                <div className="d-flex flex-row">
                    {bandCover}
                    <div className="d-flex flex-column">
                        <div style={{height: "10vh"}}>

                        </div>

                        <div className="d-flex flex-column ms-2">
                            <div style={{height: "2.5vh"}}></div>
                            <h1>
                                {band && band.name ? band.name : "Hello"}
                            </h1>
                            <h6 style={{color: "rgb(255,255,255, 0.4)"}}>
                                {band && band.year_founded ? band.year_founded : ""}
                                {
                                    band && band.bandLabelName ?
                                        <>
                                            <>&nbsp;&bull;</>
                                            <> {band.bandLabelName.name}</>
                                        </>
                                        : ""
                                }
                            </h6>
                        </div>
                    </div>
                </div>
                <div className="mt-5">
                    {band ? releaseView() : "Nothing"}
                </div>
            </div>


        </div>
    )
}