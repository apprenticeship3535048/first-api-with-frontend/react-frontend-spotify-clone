
import 'bootstrap/dist/css/bootstrap.min.css'
import './login.css'
import Login from "./dashboard/spotifyComponents/authentification/login";
import Dashboard from "./dashboard/Dashboard";


const code = new URLSearchParams(window.location.search).get('code')

const App = () => {
    return code ? <Dashboard code={code} /> : <div id="loginContainer"><Login/></div>;
    //return  <Dashboard code={code} />

}

export default App;
