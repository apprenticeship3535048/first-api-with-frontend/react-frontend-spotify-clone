import {useState, useEffect} from "react";
import useAuth from "./spotifyComponents/authentification/useAuth";
import {Container, Form} from "react-bootstrap";
import SpotifyWebApi from "spotify-web-api-node";
import TrackSearchResult from "./spotifyComponents/search/TrackSearchResult";

import axios from "axios";
import SpotifyPlayerCustom from "./spotifyComponents/SpotifyPlayerCustom";
import SideBar from "../sidebar/SideBar";
import Body from "./spotifyComponents/search/Body";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import TableView from "../../Entities/tables/TableView";
import {EntityTypes} from "./ENUM/EntityTypes";
import Band from "../../Entities/tables/bandSingleView/Band";

const spotifyWebApi = new SpotifyWebApi({
    clientId: "dbc64421c44d43bda92ee612c9f9904c"
})

export default function Dashboard({code}) {

    const accessToken = useAuth(code)
    const [search, setSearch] = useState('')
    const [searchResults, setSearchResults] = useState([])
    const [isPaused, setIsPaused] = useState(true)
    const [lyrics, setLyrics] = useState("")
    const [playingTrack, setPlayingTrack] = useState();
    const [title, setTitle] = useState("title")
    const [artist, setArtist] = useState("Artist")
    const [albumCover, setAlbumCover] = useState()
    const [player, setPlayer] = useState()
    const [duration, setDuration] = useState()
    const [trackMSPosition, setTrackMSPosition] = useState()
    let playerInstance = null;
    let interval;
    let array;

    useEffect(() => {
        if (!accessToken) return console.log("no token")
        spotifyWebApi.setAccessToken(accessToken)
        window.onSpotifyWebPlaybackSDKReady = () => {

            playerInstance = new window.Spotify.Player({
                name: 'Web Playback SDK Quick Start Player',
                getOAuthToken: cb => {
                    cb(accessToken);
                },
                volume: 0.2
            })
            setPlayer(playerInstance)
            playerInstance.addListener('ready', (device_id) => {
                array = [device_id.device_id]

                interval = setInterval(() => {
                    spotifyWebApi.transferMyPlayback(array, {
                        "play": false
                    }).then(() => {
                        console.log("device selected, ready to play music")

                        clearInterval(interval)

                    }).catch(reason => {
                        console.log(reason)
                    })
                }, 2500)

            })

            playerInstance.addListener('not_ready', ({device_id}) => {
                console.log('Device ID has gone offline', device_id)
            })
            playerInstance.addListener('initialization_error', ({message}) => {
                console.error(message);
            });

            playerInstance.addListener('authentication_error', ({message}) => {
                console.error(message);
            });

            playerInstance.addListener('account_error', ({message}) => {
                console.error(message);
            });
            playerInstance.addListener('player_state_changed', ({   paused,
                                                                    position,
                                                                    duration,
                                                                    track_window: {current_track}
                                                                }) => {
                setTrackMSPosition(position);
                setDuration(duration);
                setIsPaused(paused);
                if (title !== current_track.name) {
                    setTitle(current_track.name)
                    setArtist(current_track.artists[0].name)
                    setAlbumCover(current_track.album.images[0].url)
                }
            });

            playerInstance.connect().then(() => {
                console.log("connected to spotify,selecting device...")
            })
        }
    }, [accessToken])



    function chooseTrack(track) {
        setPlayingTrack(track)
        setArtist(track.artist)
        setTitle(track.name)
        setSearch("")
        let uris = [track.uri]
        console.log("uri: ", uris)
        spotifyWebApi.play({uris}).then(response => {
            console.log("response: ", response.body)
        })
    }

    useEffect(() => {
        if (!title) return;
        axios.get('http://localhost:3001/lyrics', {
            params: {
                artist: artist,
                track: title
            }
        }).then(res => {
            setLyrics(res.data.lyrics)
        })
    }, [title, playingTrack])

    useEffect(() => {
        if (!search) return setSearchResults([]);
        if (!accessToken) return console.log("no access token");
        let cancel = false;
        spotifyWebApi.searchTracks(search).then(
            res => {
                if (cancel) return;
                setSearchResults(res.body.tracks.items.map(track => {
                    const smallestAlbumImage = track.album.images.reduce(
                        (smallest, image) => {
                            if (image.height < smallest.height) return image
                            return smallest
                        }, track.album.images[0]
                    )
                    return {
                        artist: track.artists[0].name,
                        title: track.name,
                        uri: track.uri,
                        albumUrl: smallestAlbumImage.url,
                        duration: track.duration_ms
                    }
                }))
            })
        return () => cancel = true
    }, [search, accessToken])

    return (
        <BrowserRouter>
            <div className="d-flex flex-column">
                <div className="d-flex flex-row">
                    <SideBar></SideBar>
                    <div className="d-flex flex-column" style={{width: "calc(100vw - 150px)"}}>
                        <div className="d-flex flex-column">
                            <div
                                className="bg-darkish boxShadow"
                                style={{
                                    minHeight: "10vh"
                                }}>
                                <Form.Control
                                    type="search"
                                    placeholder="search Songs/Artists"
                                    style={{width: "20vw", height: "5vh"}}
                                    className="mt-3 ms-4"
                                    value={search}
                                    onChange={e => setSearch(e.target.value)}
                                />
                            </div>

                            <Routes>
                                <Route index element={
                                    <Body className="flex-column" searchResults={searchResults} lyrics={lyrics}
                                          chooseTrack={chooseTrack}></Body>
                                }/>
                                <Route path="/labels" element={
                                    <TableView entity={EntityTypes.Label}/>
                                }/>
                                <Route path="/bands" element={
                                    <TableView entity={EntityTypes.Band}/>
                                }/>
                                <Route path="/releases" element={
                                    <TableView entity={EntityTypes.Release}/>
                                }/>
                                <Route path="/songs" element={
                                    <TableView entity={EntityTypes.Song}/>
                                }/>
                                <Route path="/genres" element={
                                    <TableView entity={EntityTypes.Genre}/>
                                }/>
                                <Route path="/members" element={
                                    <TableView entity={EntityTypes.Member}/>
                                }/>
                                <Route path="/roles" element={
                                    <TableView entity={EntityTypes.Role}/>
                                }/>
                                <Route path="/single" element={
                                    <Band entity={EntityTypes.Role}
                                          spotifyWebApi={spotifyWebApi}
                                          chooseTrack={chooseTrack}

                                    />
                                }/>
                            </Routes>
                        </div>
                    </div>
                </div>
                <SpotifyPlayerCustom
                    accessToken={accessToken}
                    player={player}
                    albumCover={albumCover}
                    title={title}
                    artist={artist}
                    duration={duration}
                    position={trackMSPosition}
                    setPosition={setTrackMSPosition}
                    isPaused={isPaused}
                />

            </div>
        </BrowserRouter>
    )
}