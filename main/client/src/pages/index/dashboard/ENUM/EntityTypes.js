export const EntityTypes = {
    Label: {
        type: "Label",
        columns: [
            {
                columnTitle: "Name",
                columnType: "text",
                required: true
            }
        ],
        postValue(foreignKey, name){
            return {
                name: name,
            }
        },
        parent: null
    },

    Band: {
        type: "Band",
        columns: [
            {
                columnTitle: "Name",
                columnType: "text",
                required: true
            },
            {
                columnTitle: "Year Founded",
                columnType: "number",
                min: 1800,
                max: new Date().getFullYear(),
                required: true
            }
        ],
        postValue(foreignKey, name, yearFounded){

            return (foreignKey != null)? {
                name: name,
                yearFounded: yearFounded,
                label: { id: foreignKey }
            } : {
                name: name,
                yearFounded: yearFounded,
            }
        },
        parent: "Label"
    },

    Member: {
        type: "Member",
        columns: [
            {
                columnTitle: "Name",
                columnType: "text",
                required: true
            }
        ],
        postValue(foreignKey, name){
            return {
                name: name,
            }
        },
        parent: null
    },

    Role: {
        type: "Role",
        columns: [
            {
                columnTitle: "Role",
                columnType: "text",
                required: true
            }
        ],
        postValue(foreignKey, name){
            return {
                name: name,
            }
        },
        parent: null
    },

    Release: {
        type: "Release",
        columns: [
            {
                columnTitle: "Name",
                columnType: "text",
                required: true
            },
            {
                columnTitle: "Number of Sales",
                columnType: "number",
                min: 0,
                max: 999999999,
                required: false
            },
            {
                columnTitle: "Release Year",
                columnType: "number",
                min: 1800,
                max: new Date().getFullYear(),
                required: false
            },
            {
                columnTitle: "Type of Release",
                columnType: "radio",
                radioOptions: [
                    {name: "EP", value: "Extended_Play", checked: false},
                    {name: "Album", value: "Album", checked: true},
                    {name: "Single", value: "Single", checked: false},
                ],
                required: true
            }
        ],
        postValue(foreignKey, name, numberOfSales, release_year, typeOfRelease){
            return  (foreignKey != null)? {
                name: name,
                numOfSales: numberOfSales,
                releaseYear: release_year,
                typeOfRelease: typeOfRelease,
                label: {
                    id: foreignKey
                }
            } : {
                name: name,
                numOfSales: numberOfSales,
                releaseYear: release_year,
                typeOfRelease: typeOfRelease,
            }
        },
        parent: "Label"
    },

    Genre: {
        type: "Genre",
        columns: [
            {
                columnTitle: "Name",
                columnType: "text",
                required: true
            }
        ],
        postValue(foreignKey, name){
            return {
                name: name,
            }
        },
        parent: null
    },

    Song: {
        type: "Song",
        columns: [
            {
                columnTitle: "Name",
                columnType: "text",
                required: true
            },
            {
                columnTitle: "Length in Seconds",
                columnType: "number",
                min: 0,
                max: 999999,
                required: true
            },
        ],
        postValue(foreignKey, name, length){
            return (foreignKey != null)? {
                name: name,
                length: length,
                releases: {
                    id: foreignKey,
                }
            } : {
                name: name,
                length: length
            }
        },
        parent: "Release"
    },
}