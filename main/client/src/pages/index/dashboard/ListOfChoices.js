import "./ui/EditDropDown.css"
import React from "react";

function ListOfChoices(props){

    const className = "choice"

    function getList() {
        if (props.options != null) {
            if(props.options.length > 0) {

                return (
                    props.options.map(option => (
                        <input type="button" className={className} key={Math.random()} onClick={props.onClick}
                           value={option.name}/>
                    ))
                )
            }
        }
    }

    return (
        <div id={props.id} className="choiceList ms-2 mt-2">
            <div className="list">
                {getList()}
            </div>
        </div>
    )
}

export default ListOfChoices;