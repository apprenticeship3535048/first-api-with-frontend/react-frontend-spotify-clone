import React, {useEffect, useState} from "react";
import Script from 'react-load-script'
import PlayerStylesheet from "./Player-Stylesheet.css"
import MusicProgressBar from "./controls/MusicProgressBar";
import PlayerControl from "./controls/PlayerControl";
import VolumeSlider from "./controls/VolumeSlider";


export default function SpotifyPlayerCustom({
                                                accessToken,
                                                artist,
                                                title,
                                                player,
                                                albumCover,
                                                position,
                                                duration,
                                                isPaused,
                                                setPosition
                                            }) {
    const [newPos, setNewPos] = useState()

    useEffect(() => {
        if (player === null || !newPos) return
        player.seek(newPos).then(console.log("yay"))
    }, [newPos])




    if (!accessToken) return null;
    return (

        <div className="fixed-bottom">
            <div className="d-flex flex-row  boxShadow blackBorderTop align-items-center"
                 style={{backgroundColor: "#2b2b2b", height: "10vh"}}>
                <Script url="https://sdk.scdn.co/spotify-player.js"></Script>


                <div>
                    <img src={albumCover} style={{height: "64px", width: "64px"}}/>
                </div>
                <div className="ms-2 mt-2 font-monospace text-white d-flex flex-column w-25">
                    <a id="title" style={{fontSize: "1em"}}>{title}</a>
                    <a id="artist" style={{fontSize: "0.75em"}}>{artist}</a>
                </div>
                <div className="d-flex flex-column" style={{margin: "0 0 0 12vw"}}>
                    <PlayerControl
                        player={player}
                        setPosition={setPosition}
                        isPaused={isPaused}
                    />
                    <MusicProgressBar
                        position={position}
                        duration={duration}
                        isPaused={isPaused}
                        setNewPos={setNewPos}
                    />

                </div>
                <VolumeSlider player={player}/>
            </div>
        </div>);
}




