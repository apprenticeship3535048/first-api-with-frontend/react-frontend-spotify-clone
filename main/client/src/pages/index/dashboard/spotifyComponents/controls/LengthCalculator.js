export function LengthCalculator(totalSeconds) {
    const minutes = Math.floor(totalSeconds / 60)
    const seconds = totalSeconds % 60
    let formattedSeconds

    if (seconds < 10) formattedSeconds = "0" + seconds
    else formattedSeconds = seconds

    return (minutes + ":" + formattedSeconds)
}