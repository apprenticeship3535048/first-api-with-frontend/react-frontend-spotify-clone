import React, {useEffect, useState} from 'react';
import { ProgressBar } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import shadow from "../../shadow.css"
import {LengthCalculator} from "./LengthCalculator";
function MusicProgressBar({duration, position, isPaused, setNewPos}) {

    const [updateProgress, setUpdateProgress] = useState()
    const [progress, setProgress] = useState(0)
    const [maxValue, setMaxValue] = useState(0)
    const [circleX, setCircleX] = useState(0)
    const [circleY, setCircleY] = useState(0)
    const [visibility, setVisibility] = useState("hidden")



    useEffect(() => {
        if (!duration) return
        let durationSeconds = Math.floor(duration /1000)
        setMaxValue(durationSeconds)
    }, [duration])

    useEffect(() => {
        let currentValue = (position /1000).toFixed(0)
        setProgress( () => {return parseInt(currentValue)})

    }, [position])

    useEffect(() => {
        if (!progress) return
            const progressBar = document.getElementById("progress-bar")
            setCircleY(progressBar.offsetTop-2)
            //console.log("progress: ", progress)
            const progressBarWidth = document.documentElement.clientWidth / 100 * parseInt(progressBar.style.width)
            const posXNowPercent = progress / maxValue * 100
            const posXNow = progressBarWidth / 100 * posXNowPercent + progressBar.offsetLeft
        setCircleX(posXNow)
        document.getElementById("progressCircle").style.left = ""+posXNow+""
    }, [progress, document.documentElement.clientWidth])

    useEffect(() =>{

        if (!isPaused) setUpdateProgress(setInterval(() =>{
            setProgress(s => {
               const updatedProgress = s+1
                return updatedProgress
            })
        }, 1000))

    }, [isPaused])

    useEffect(() =>{
        if (isPaused) clearInterval(updateProgress)
    }, [isPaused, duration])

    const click = (e) => {
        const progressBarWidth = document.documentElement.clientWidth / 100 * parseInt(e.target.style.width)
        const newPosPercent = Math.round(e.nativeEvent.offsetX / progressBarWidth * 100)
        const newPos = Math.round(duration / 100 * newPosPercent)
        setNewPos(newPos)
    }

    const hover = (e) => {

        setVisibility("visible")
        document.getElementById("progress-bar").children[0].className = "progress-bar bg-neonPink"
    }
    const leave = (e) => {
        setVisibility("hidden")
        document.getElementById("progress-bar").children[0].className = "progress-bar bg-whitesmoke"
    }


    return (
            <div className="d-flex flex-row mt-1 align-items-center" style={{color:"whitesmoke"}}>
                <div className="me-2">{LengthCalculator(progress)}</div>

            <ProgressBar
                id="progress-bar"
                now={progress}
                max={maxValue}
                style={{height:"5px", width:"20vw", backgroundColor:"grey"}}
                variant="whitesmoke"
                onClick={click}
                onMouseOver={hover}
                onMouseLeave={leave}
            ></ProgressBar>
                <div style={{
                    backgroundColor: "whitesmoke",
                    width: "10px",
                    height: "10px",
                    borderRadius: "50%",
                    position: "absolute",
                    left: circleX,
                    top: circleY,
                    visibility: visibility
                    }} id="progressCircle"
                     onMouseOver={hover}
                     onMouseLeave={leave}
                ></div>
                <div className="ms-2"> {LengthCalculator(maxValue)}</div>

            </div>
    );
}
export default MusicProgressBar;