import React from "react";
export default function PlayerControl ({player, setPosition, isPaused}){
    function playOrPauseMusic() {
        if (player === null) return console.log("no player")
        player.togglePlay()

    }

    function playNext() {
        if (player === null) return
        player.nextTrack()
        setPosition(0)

    }

    function playPrevious() {
        if (player === null) return
        player.previousTrack()
        setPosition(0)
    }
    return (

        <div className="d-flex flex-row justify-content-center">
            <a className="previous mt-2 me-2" onClick={playPrevious}></a>
            <button
                className={`m-1 play-pause ${isPaused ? "paused" : "play"}`}
                onClick={playOrPauseMusic}>

            </button>
            <a className="next mt-2 ms-2" onClick={playNext}></a>
        </div>
    )
}