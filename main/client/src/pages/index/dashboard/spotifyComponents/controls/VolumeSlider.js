import React from "react";

export default function VolumeSlider({player}) {

    const regulateVolume = (e) =>{
        const newVolume = e.currentTarget.value / 100
        player.setVolume(newVolume)
    }


    return (
        <div className="sliderContainer" style={{position:"fixed", right:"20px"}}>
            <input type="range"
                   min="0"
                   max="100"
                   className="slider"
                   id="volumeSlider"
                   onInput={regulateVolume}
            />
        </div>
    )
}