import shadow from "../../shadow.css"
import TrackSearchResult from "./TrackSearchResult";
export default function Body ({searchResults, lyrics, chooseTrack}) {


    return (
        <div
            style=
                {{
                    backgroundColor: "#2b2b2b",
                    width: "calc(100vw - 150px)",
                    height: "80vh",
                    overflowY: "auto"
                }}

            className="boxShadow blackBorderTop">

            {searchResults.map(track => (
                <TrackSearchResult
                    track={track}
                    key={track.uri}
                    chooseTrack={chooseTrack}/>
            ))}
            {searchResults.length ===0 && (<div className="text-center text-white" style={{whiteSpace: "pre-wrap"}}>{lyrics}</div>)}
        </div>
    )
}
