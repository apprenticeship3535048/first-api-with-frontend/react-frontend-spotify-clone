import React from "react";

export default function TrackSearchResult({track, chooseTrack}) {
    function handleplay(){
        chooseTrack(track)
    }
    return (
        <div className="d-flex m-2 align-items-center text-white"
        style={{cursor: "pointer"}}
             onClick={handleplay}
        >
            <img src={track.albumUrl} style={{height: "64px", width: "64px"}}/>
            <div className="ml-3 ">
                <div>{track.title}</div>
                <div>{track.artist}</div>
            </div>
        </div>
    )
}