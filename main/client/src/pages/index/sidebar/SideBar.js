import shadow from "../dashboard/shadow.css"
import sidebar from "./sidebar.css"
import {Link} from "react-router-dom";

export default function SideBar() {


    return (
        <div
            style={{minWidth: "150px"}}
            className="blackBorderRight boxShadow d-flex flex-column bg-darkish"
        >
            <div className="border-b-1-black boxShadow-white-b boxShadow-black-t "
                 style={{minHeight: "10vh", paddingBottom:"1vh"}}>
                <h2 className="ms-2">
                    <Link
                        to="" className="sidebar admin-user-view">
                        <div>ADMIN VIEW</div>
                    </Link>
                </h2>
            </div>
            <div className="d-flex flex-column boxShadow-black-t ps-2" style={{height: "80vh"}}>
                <Link to="/labels" className="sidebar entry mt-2 ">Labels</Link>
                <Link to="/bands" className="sidebar entry mt-1">Bands</Link>
                <Link to="/releases" className="sidebar entry mt-1">Releases</Link>
                <Link to="/songs" className="sidebar entry mt-1">Songs</Link>
                <Link to="/genres" className="sidebar entry mt-1">Genres</Link>
                <Link to="/members" className="sidebar entry mt-1">Members</Link>
                <Link to="/roles" className="sidebar entry mt-1">Roles / Instruments</Link>
                <Link to="/single" className="sidebar entry mt-1">Single test</Link>
                <Link to="" className="sidebar entry mt-1">back</Link>
            </div>
        </div>
    )
}
