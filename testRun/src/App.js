import "./index.css"
import Band from "./components/tables/Band";

function App() {
    return (
        <div className="fullSize">
            <Band/>
        </div>
    );
}

export default App;
