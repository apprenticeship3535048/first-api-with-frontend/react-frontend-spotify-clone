import Box from "../ui/Box";
import "./EditBox.css";
import "../ui/GeneralClasses.css";
import EditList from "./EditList";
import AddButton from "../ui/AddButton";
import DeleteButton from "../ui/DeleteButton";

function EditBox(props){
    const isCreating = props.list.useState.creating;

    const deleteButtonHandler = () => {
        props.list.useState.setCreating(false)
    };

    const addButtonHandler = () => {
        props.list.useState.setCreating(true);
    }

    const tableInput = props.list.tableInput;
    const handler = () => {
        props.list.handler.addTable();
    }

    return (
        <Box className="editBox centerAll">
            <h1 className="header">Add A New Band</h1>

            {!isCreating &&
                <AddButton onClick={addButtonHandler}/>
            }
            {isCreating &&
                <div>
                    <EditList information={tableInput} onChange={props.list.onChangeHandler}/>
                    <div className="buttonBox">
                        <DeleteButton className="smallButton" onClick={deleteButtonHandler}/>

                        <AddButton className="smallButton" onClick={handler} />
                    </div>
                </div>
            }
        </Box>
    )
}

export default EditBox;