import "./EditDropDown.css"
import "../ui/GeneralClasses.css"
import DropDownMenu from "../ui/DropDownMenu";
import ListOfChoices from "../ui/ListOfChoices";

function EditDropDown(props){
    const selectChoiceHandler = event => {
        document.getElementById(selectId).value = event.target.value;
        document.getElementById(listId).style.display = "none";
    }

    const selectId = props.id + "";
    const listId = props.id + 1  + "";

    return (
        <div className="input editEntityElement" style={{zIndex: 200}}>
            <DropDownMenu listId={listId} selectId={selectId}/>
            <ListOfChoices id={listId} name={props.name} choices={props.choices} onClick={selectChoiceHandler} nullValue={props.nullValue}/>
        </div>
    )
}

export default EditDropDown;