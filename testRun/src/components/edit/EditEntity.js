import "./EditEntity.css"
import EditDropDown from "./EditDropDown";
import {EditEntityTypes} from "./EditEntityTypes";
import EditSearch from "./EditSearch";


function EditEntity(props) {
    const className = "editEntity thickOutline " + props.className;

    function OutPutInputField() {
        if (props.type === EditEntityTypes.number) {
            return <input type="number" min={props.min} max={props.max} name={props.name} id={props.id}
                          className="thinOutline editEntityElement input" placeholder={props.placeholder}/>

        } else if (props.type === EditEntityTypes.dropDown) {
            return <EditDropDown choices={props.choices} name={props.name} id={props.id} nullValue={props.nullValue}/>

        } else if (props.type === EditEntityTypes.search) {
            return <EditSearch choices={props.choices} name={props.name} id={props.id} onChange={props.onChange} nullValue={props.nullValue}/>
        } else {
            return <input type="text" className="editEntityElement input thinOutline"
                          name={props.name} placeholder={props.placeholder} id={props.id}/>
        }
    }

    return (
        <div className={className}>
            <div className="editEntityElement centerAll editEntityTitleBox">
                {props.title}:
            </div>
            { OutPutInputField() }
        </div>
    )
}

export default EditEntity;