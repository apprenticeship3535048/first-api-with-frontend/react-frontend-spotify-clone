import EditEntity from "./EditEntity";
import "./EditList.css"
import "../ui/GeneralClasses.css";

function EditList(props) {
    if(props.information.length === 0) {
        return <h2>No titles found</h2>;
    }

    return (
        <ul className="centerAll">
            {props.information.map(information => (
                    <EditEntity
                        key={information.id}
                        id={information.id}
                        title={information.title}
                        type={information.type}
                        placeholder={information.placeholder}
                        name={information.name}
                        min={information.min}
                        max={information.max}
                        choices={information.choices}
                        nullValue={information.nullValue}
                        onChange={props.onChange}/>
                    )
                )
            }
        </ul>
    );
}

export default EditList;