import "./EditDropDown.css"
import "../ui/DropDownMenu.css"
import "../ui/GeneralClasses.css"
import SearchButton from "../ui/SearchButton";
import EditSearchMenu from "./EditSearchMenu";

function EditSearch(props) {
    const selectChoiceHandler = event => {
        document.getElementById(selectId).value = event.target.value;
        document.getElementById(selectId).placeholder = event.target.placeholder;
        document.getElementById(listId).style.display = "none";
    }

    const selectId = props.id + "";
    const listId = props.id + 1 + "";

    return (
        <div className="input editEntityElement" style={{zIndex: 200}}>
            <SearchButton listId={listId} selectId={selectId}/>
            <EditSearchMenu id={listId} choices={props.choices} onChange={props.onChange}
                            name={props.name} onClick={selectChoiceHandler} nullValue={props.nullValue}/>
        </div>
    );
}

export default EditSearch;