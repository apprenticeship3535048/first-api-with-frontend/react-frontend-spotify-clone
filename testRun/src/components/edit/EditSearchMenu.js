import "./EditDropDown.css"
import "./EditSearchMenu.css"
import "./EditEntity"
import "../ui/GeneralClasses.css"

import ListOfChoices from "../ui/ListOfChoices";
import Window from "../ui/Window";
import {useState} from "react";

function EditSearchMenu(props){
    const onClickHandler = event => {
        document.getElementById(props.id + 2 + "").style.display = "none";
        document.getElementById(props.id + 4 + "").style.display = "none";
        props.onClick(event);
    }

    let [listOfNames, setListOfNames] = useState(props.choices.choiceName)
    let [listOfIds, setListOfIds] = useState(props.choices.choiceId)

    const onChangeHandler = event => {
        props.onChange.createListOfChoices(event.target.value, setListOfNames, setListOfIds)
    }

    return (
        <Window>
            <div className="searchMenu thickOutline center" id={props.id + 2 + ""}>
                <div>
                    <div className="bottomBorder searchMenuHeader thickOutline"> Add Label </div>

                    <div className="editEntity thickOutline searchField bottomBorder">
                        <input type="text" className="editEntityElement input thinOutline searchFieldEntity" onChange={onChangeHandler}
                           placeholder="Search for Label" id={props.id + 3 + ""} />
                    </div>
                </div>

                <ListOfChoices className="pointerEventOn" id={props.id + ""} name={props.name} nullValue={props.nullValue}
                               childrenClass="leftMargin" choices={ {choiceName: listOfNames, choiceId: listOfIds} } onClick={onClickHandler}/>
            </div>
            <div className="editEntity thickOutline searchMenuFooter" id={props.id + 4 + ""}>
                <button className="editEntityElement createButton" text="Create New Label" id={props.id + 5} />
            </div>
        </Window>
    )
}

export default EditSearchMenu;