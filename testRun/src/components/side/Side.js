import EditBox from "../edit/EditBox";
import "./Side.css"
import "../../index.css"

function Side(props) {
    return (
        <div className="sideCenter fullSize">
            <EditBox titles={props.titles}/>
        </div>
    )
}

export default Side;