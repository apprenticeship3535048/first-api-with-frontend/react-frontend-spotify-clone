import React, {useEffect, useState} from "react";
import EditBox from "../edit/EditBox";
import {EditEntityTypes} from "../edit/EditEntityTypes";
import "../side/Side.css"
import "../../index.css"

function Band() {
    const id = Math.random();
    const MIN_YEAR = 1800;
    const MAX_YEAR = 2023;

    const addTable = () => {
        const name = document.getElementById(id + "").value;
        const year = document.getElementById(id + 1 + "").value;
        const labelName = document.getElementById(id + 2 + "").value;
        const labelId = document.getElementById(id + 2 + "").placeholder;

        if (name !== "" && name != null) {
            document.getElementById(id + "").style.backgroundColor = "white";
            if (year >= MIN_YEAR && year <= MAX_YEAR) {
                document.getElementById(id + 1 + "").style.backgroundColor = "white";

                fetch('http://localhost:8080/api/bands', {
                    method: 'post',
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify({
                        name: name,
                        yearFounded: year,
                        label: {
                            "id": labelId,
                            "name": labelName
                        },
                    })
                }).then(response => response.json()).then(data => {
                    console.log(data.id)
                })

                setIsCreating(false);
            } else {
                document.getElementById(id + 1 + "").style.backgroundColor = "#e56868";
            }
        } else {
            document.getElementById(id + "").style.backgroundColor = "#e56868";
        }
    }

    const [choiceListName, setChoiceListName] = useState(["No Label"]);
    const [choiceListId, setChoiceListId] = useState([null]);

    const BAND_INPUT = [
        {
            title: "Name",
            placeholder: "Iron Maiden",
            type: EditEntityTypes.text,
            name: "bandName",
            id: id
        },
        {
            title: "Year founded",
            placeholder: "1975",
            type: EditEntityTypes.number,
            min: MIN_YEAR,
            max: MAX_YEAR,
            name: "year_founded",
            id: id + 1
        },
        {
            title: "Label",
            placeholder: "select",
            nullValue: "No Label",
            type: EditEntityTypes.search,
            choices: {
                choiceName: choiceListName,
                choiceId: choiceListId
            },
            name: "label_id",
            id: id + 2
        },
    ]

    const fetchChoiceList = search => fetch("http://localhost:8080/api/labels/" +
        ((search != null && search !== "") ? ("mapped-search?name=" + search + "&") : "mapped?") + "limit=10&page=1&select=0&childlimit=10&child_page=0", {
        method: 'get',
        headers: {'Content-Type': 'application/json'},

    }).then(response => response.json()).then(labelList => {
        return (labelList.content != null)? labelList.content : labelList.results;
    })

    const [isCreating, setIsCreating] = React.useState(false);

    const createListOfChoices = (search, setChoiceNames, setChoiceIds) => {
        let choiceNameArray = [];
        let choiceIdArray = [];

        fetchChoiceList(search).then(content => {

            content.forEach(label => {
                choiceNameArray.push(label.name)
                choiceIdArray.push(label.id)
            })

            if (choiceNameArray.length > 0) {
                setChoiceNames(choiceNameArray);
                setChoiceIds(choiceIdArray)
            } else {
                setChoiceNames(["No Label"])
                setChoiceIds([null])
            }
        })
    };

    useEffect(() => {
        if (isCreating) {
            createListOfChoices(null, setChoiceListName, setChoiceListId)
        }
    }, [isCreating]);

    return (
        <div className="sideCenter fullSize">
            {<EditBox list={{
                useState: {
                    creating: isCreating,
                    setCreating: setIsCreating
                },
                handler: {addTable},
                onChangeHandler: {createListOfChoices},
                tableInput: BAND_INPUT
            }}/>}
        </div>
    )
}

export default Band;