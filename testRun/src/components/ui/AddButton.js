import "./AddButton.css"
import "./GeneralClasses.css"

function AddButton(props) {
    const onClickHandler = () => {
        props.onClick();
    }

    const className = "addButton centerAll button " + props.className;
    return <button onClick={onClickHandler} className={className}>
        <div>+</div>
    </button>
}

export default AddButton;