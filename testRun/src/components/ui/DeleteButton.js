import "./DeleteButton.css"
import "./GeneralClasses.css"

function DeleteButton(props) {
    const onClickHandler = () => {
        props.onClick();
    }

    const className = "deleteButton centerAll button " + props.className;
    return <button onClick={onClickHandler} className={className}>
        <div className="zeroMargin">X</div>
    </button>
}

export default DeleteButton;