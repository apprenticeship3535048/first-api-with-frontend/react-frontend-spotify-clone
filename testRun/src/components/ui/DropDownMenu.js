import "./DropDownMenu.css"
import "./GeneralClasses.css"

function DropDownMenu(props) {
    const listId = props.listId;
    const selectId = props.selectId;

    const clickDropButtonHandler = () => {
        const list = document.getElementById(listId);
        if(list.style.display === "block") {
            document.getElementById(listId).style.display = "none";
        } else {
            document.getElementById(listId).style.display = "block";
        }
    }

    return (
        <div>
            <div className="dropDownButton centerAll" onClick={clickDropButtonHandler}><p>v</p></div>
            <input type="text" className="choiceBoxOutPut" disabled={true} placeholder="Select" id={selectId}/>
        </div>
    );
}

export default DropDownMenu;