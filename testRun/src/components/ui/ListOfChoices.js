import "../edit/EditDropDown.css"
import "./GeneralClasses.css"
import React from "react";

function ListOfChoices(props){
    const className = "choiceList alignColumn " + props.className;
    const childrenClass = "choice thinOutline " + props.childrenClass;

    return (
        <div className={className} id={props.id}>
            {(props.nullValue != null) && <input type="button" className={childrenClass} onClick={props.onClick} name={props.name}
                    value={props.nullValue}/>}
            {props.choices.choiceName != null &&
                props.choices.choiceName.map(choice => (
                    <input type="button" className={childrenClass} key={Math.random()}
                           onClick={props.onClick} name={props.name} value={choice}
                           placeholder={props.choices.choiceId[props.choices.choiceName.findIndex(name => name === choice)] + ""}
                    />
                ))
            }
        </div>
    )
}

export default ListOfChoices;