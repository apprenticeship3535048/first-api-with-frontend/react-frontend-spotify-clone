import "./DropDownMenu.css"
import "./GeneralClasses.css"

function SearchButton(props) {
    const listId = props.listId;
    const selectId = props.selectId;

    const clickDropButtonHandler = () => {
        const list = document.getElementById(listId + "");
        const listWindowUpperHalf = document.getElementById(listId + 2 + "");
        const listWindowUnderHalf = document.getElementById(listId + 4 + "");

        if(list.style.display === "flex") {
            list.style.display = "none";
            listWindowUpperHalf.style.display = "none";
            listWindowUnderHalf.style.display = "none";
        } else {
            list.style.display = "flex";
            listWindowUpperHalf.style.display = "flex";
            listWindowUnderHalf.style.display = "flex";
        }
    }

    return (
        <div>
            <div className="dropDownButton centerAll" onClick={clickDropButtonHandler}><p>O</p></div>
            <input type="text" className="choiceBoxOutPut" value="Search" disabled={true} placeholder="0" id={selectId}/>
        </div>
    );
}

export default SearchButton;