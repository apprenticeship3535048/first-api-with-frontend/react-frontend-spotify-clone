import "./Window.css"
import "./GeneralClasses.css"

function Window(props) {
    const className = "window " + props.className;

    return (
        <div className={className} id={props.id}>
            <div className="innerWindow centerAll">
                {props.children}
            </div>
        </div>
    )
}

export default Window;